# the_graph

Dart Polymer wrapper of [The-Graph](https://github.com/the-grid/the-graph). Actually the [fork][the-graph-fork] is used.   

## Usage

A simple usage example:

    import 'package:the_graph/the_graph.dart';

    main() {
      var awesome = new Awesome();
    }

## Features and bugs

Please file feature requests and bugs at the [issue tracker][tracker].

[tracker]: http://example.com/issues/replaceme

[the-graph-fork]: https://github.com/kelegorm/the-graph