import 'package:polymer/polymer.dart';

import 'dart:html';
import 'dart:async';

@CustomTag('my-wrapper')
class MyWrapper extends PolymerElement {

  MyWrapper.created() : super.created() {

  }

  @override
  void attached() {
    super.attached();

    var theGraph = $["theGraph"];

    theGraph.graph = {
        "processes": {
            "BooleanPlay1": {
                "component": "animations/BooleanPlay",
                "metadata": {
                    "label":"BooleanPlay1",
                    "x": 360,
                    "y": 480
                }
            },
            "Label1": {
                "component": "widgets/Label",
                "metadata": {
                    "label":"Label1",
                    "x": 540,
                    "y": 480
                }
            }
        },
        "connections": [
            {
                "data": "Running",
                "tgt": {
                    "process": "BooleanPlay1",
                    "port": "truevalue"
                }
            },
            {
                "data": "Stopped",
                "tgt": {
                    "process": "BooleanPlay1",
                    "port": "falsevalue"
                }
            },
            {
                "src": {
                    "process": "BooleanPlay1",
                    "port": "out"
                },
                "tgt": {
                    "process": "Label1",
                    "port": "text"
                }
            }
        ]
    };

    window.onResize.listen((e) {
      theGraph.width = clientWidth;
      theGraph.height = clientHeight;
    });

    theGraph.width = clientWidth;
    theGraph.height = clientHeight;
  }

  void editor_nodesSelected (e, details) {
    print('Nodes Selected: $e, $details');
  }
}