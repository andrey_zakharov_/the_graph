// DO NOT EDIT: auto-generated with `pub run custom_element_apigen:update`

/// Dart API for the polymer element `the_graph_editor`.
library the_graph.the_graph_editor;

import 'dart:convert';
import 'dart:html';
import 'dart:async' show Future;
import 'dart:js';
import 'package:web_components/interop.dart' show registerDartType;
import 'package:polymer/polymer.dart' show initMethod;
import 'package:custom_element_apigen/src/common.dart' show PolymerProxyMixin, DomProxyMixin;


class TheGraphEditor extends HtmlElement with DomProxyMixin, PolymerProxyMixin {
  TheGraphEditor.created() : super.created();

  factory TheGraphEditor() => new Element.tag('the-graph-editor');

  get graph => jsElement[r'graph'];
  set graph(value) { jsElement[r'graph'] = (value is Map || value is Iterable) ? new JsObject.jsify(value) : value;}

  get grid => jsElement[r'grid'];
  set grid(value) { jsElement[r'grid'] = (value is Map || value is Iterable) ? new JsObject.jsify(value) : value;}

  get snap => jsElement[r'snap'];
  set snap(value) { jsElement[r'snap'] = (value is Map || value is Iterable) ? new JsObject.jsify(value) : value;}

  get width => jsElement[r'width'];
  set width(value) { jsElement[r'width'] = (value is Map || value is Iterable) ? new JsObject.jsify(value) : value;}

  get height => jsElement[r'height'];
  set height(value) { jsElement[r'height'] = (value is Map || value is Iterable) ? new JsObject.jsify(value) : value;}

  get autolayout => jsElement[r'autolayout'];
  set autolayout(value) { jsElement[r'autolayout'] = (value is Map || value is Iterable) ? new JsObject.jsify(value) : value;}

  get theme => jsElement[r'theme'];
  set theme(value) { jsElement[r'theme'] = (value is Map || value is Iterable) ? new JsObject.jsify(value) : value;}

  get selectedNodes => jsElement[r'selectedNodes'];
  set selectedNodes(value) { jsElement[r'selectedNodes'] = (value is Map || value is Iterable) ? new JsObject.jsify(value) : value;}

  get errorNodes => jsElement[r'errorNodes'];
  set errorNodes(value) { jsElement[r'errorNodes'] = (value is Map || value is Iterable) ? new JsObject.jsify(value) : value;}

  get selectedEdges => jsElement[r'selectedEdges'];
  set selectedEdges(value) { jsElement[r'selectedEdges'] = (value is Map || value is Iterable) ? new JsObject.jsify(value) : value;}

  get animatedEdges => jsElement[r'animatedEdges'];
  set animatedEdges(value) { jsElement[r'animatedEdges'] = (value is Map || value is Iterable) ? new JsObject.jsify(value) : value;}

  get onContextMenu => jsElement[r'onContextMenu'];
  set onContextMenu(value) { jsElement[r'onContextMenu'] = (value is Map || value is Iterable) ? new JsObject.jsify(value) : value;}

  get displaySelectionGroup => jsElement[r'displaySelectionGroup'];
  set displaySelectionGroup(value) { jsElement[r'displaySelectionGroup'] = (value is Map || value is Iterable) ? new JsObject.jsify(value) : value;}

  get forceSelection => jsElement[r'forceSelection'];
  set forceSelection(value) { jsElement[r'forceSelection'] = (value is Map || value is Iterable) ? new JsObject.jsify(value) : value;}

  /// Actual Graph model-controller.
  get nofloGraph => jsElement[r'nofloGraph'];

  get offsetY => _getTheGraphProperty(r'offsetY');
  set offsetY(num value) => _setTheGraphProperty('offsetY', value);

  get offsetX => _getTheGraphProperty(r'offsetX');
  set offsetX(num value) => _setTheGraphProperty('offsetX', value);

  List<num> get pan => jsElement.callMethod('getTheGraphProperty', ['pan']);
  set pan(List<num> value) => _setTheGraphProperty('pan', value);

  num get scale => jsElement.callMethod('getTheGraphProperty', ['scale']);
  set scale(num value) => _setTheGraphProperty('scale', value);

  void triggerAutolayout () {
    new Future.delayed( new Duration(milliseconds: 0), () =>jsElement.callMethod('triggerAutolayout'));
  }

  void addInitial(value, node, port) {
    var data = value;
    if (data is Map || data is Iterable)
      data = new JsObject.jsify(value);

    jsElement[r'nofloGraph'].callMethod('addInitial', [data, node, port]);
  }

  bool removeInitial(node, port) => jsElement[r'nofloGraph'].callMethod('removeInitial', [node, port]);

  bool setNodeMetadata(id, Map metadata) => nofloGraph.callMethod('setNodeMetadata', [id, new JsObject.jsify(metadata)]);

  void addNode(id, type, Map metadata) => jsElement.callMethod('addNode', [id, type, new JsObject.jsify(metadata)]);

  void focusNode(Map node) => jsElement.callMethod('focusNode', [new JsObject.jsify(node)]);

  void setLibrary(library) => jsElement.callMethod('setLibrary', [new JsObject.jsify(library)]);

  _getTheGraphProperty(String property) => jsElement.callMethod('getTheGraphProperty', [property]);
  _setTheGraphProperty(String property, value) => jsElement.callMethod('setTheGraphProperty', [property, (value is Map || value is Iterable) ? new JsObject.jsify(value) : value]);

  void copySelected() => jsElement.callMethod('copySelected');
  void pasteSelected() => jsElement.callMethod('pasteSelected');
  void deleteSelected() => jsElement.callMethod('deleteSelected');

  Map toJSON() {
    return JSON.decode(context['JSON'].callMethod('stringify', [jsElement.callMethod('toJSON')]));
  }
}

@initMethod
upgradeTheGraphEditor() => registerDartType('the-graph-editor', TheGraphEditor);
