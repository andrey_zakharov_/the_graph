# Setup
 
Here I'm describing how to built package from zero. If you want to make simple the-graph update, you can to refresh the-graph files only.

Before installation ensure to install node.js, bower, grunt.
 
### Install JS the-graph
If you doing a re-install js the grap, you can just totally remove all files in lib/src. We will create it again. 

#### 1. Clone the-graph 
After git clone:

git submodules init

git submodules update

This will clone [the-graph repo][the-graph-fork] into `lib/src/the-graph` folder. Or somewhere out from package folder and then copy files into `lib/src/the-graph`. Doesn't matter.
If you are cloning original the-graph repo, you should to pass all next steps. In my fork I already done all steps and you need to setup bower dependencies only, into upper folder `lib/src`, as it's described in 2-3 steps.

#### 2. Configure bower 

Just create a `.bowerrc` file in `lib/src/the-graph/` with next content:

    {
      "directory": "../"
    }
    
So, it will place all dependencies in `lib/src`, one folder up from `lib/src/the-graph`. It's a condition of `custom_element_apigen` to proper work.

#### 3. Install bower dependencies:

Here you should go to `the-graph` folder and call `bower install` from it.
    
    $ cd lib/src/the-graph/
    $ bower install
    
So, here you will have a bunch of bower packages in `lib/src`, like this:
    
    lib
      \->src
        |->core-component-page
        |->ease-djdeath
        |->font-awesome
        |->hammerjs
        |->klayjs
        |->klayjs-noflo
        |->polymer
        |->react
        |->react.animate-djdeath
        |->the-graph
        \->webcomponentsjs

#### 4. Build noflo

Skip this and follow steps if `build/noflo.js` already exists and you don't tend to update it.

The-graph uses `noflo` to work with. But we don't need noflo repo, but compiled `noflo.js`. Here we install noflo, make build and remove unnecessarily noflo files. So, we are still in `lib/src/the-graph`: 
    
    $ sudo npm install
    $ grunt build
     
After this we have a `build/noflo.js` in `lib/src/the-graph`. And grunt and npm installed a huge bunch of unnecessarily files. After build we don't need it anymore. So, remove `bower_components` and `node_modules`! 

So, now we have a worked the-grid js version. 
  
### Generate dart wrapper

Here we will generate dart polymer wrapper for js the-grid. If we have created `the_graph_editor.html` already in `lib` folder, don't remote it, because it contains a some more, than just generated content. I wrote a some fixes here: autosize from styles (not properties), mouse and something else. Just rename it to `the_graph_editor.html_old`, and then move additions to new generated file.

We use [`custom_element_apigen`](https://pub.dartlang.org/packages/custom_element_apigen) package for this. We should create a config file and then generate files.
  
#### 1. Making config

Create a file `configfile.yaml` in package root folder with follows content:
    
    files_to_generate:
      - the-graph/the-graph/the-graph.html
      - the-graph/the-graph-editor/the-graph-editor.html

#### 2. Generate wrapper 

Run `custom_element_apigen` for generate wrapper files:
   
   $ pub run custom_element_apigen:update configfile.yaml

It creates a two components in lib/the_graph folder. But lastly with errors. Don't worry, we will fix it!

All right, but it's not all. Also you need run this script without errors to Regenerate polymer.html file. Yes, this script not only generates componetns, it changes polymer bower component too! Yeah!

So, to run script without errors jusr comment second file in config file, keep `the-graph.html` alone. Then uncomment last string and keep going to next unit.

#### 3. Fix generated files
 
In `the_graph` folder we have `the_graph.html` and `the_graph.dart` files, which works well. But in `the_graph_editor` folder we have dart file only, because `custom_element_apigen` make a error here. I don't know why, but I know, how to fix it.
     
To fix it we copy `the_graph.html` file into `the_graph_editor/the_graph_editor.html`. Then we change all file names in the file to `the-graph-editor`. Was:
 
    <link rel="import" href="../../src/the-graph/the-graph/the-graph.html">
    <script type="application/dart" src="the_graph.dart"></script>

became:
 
    <link rel="import" href="src/the-graph/the-graph-editor/the-graph-editor.html">
    <script type="application/dart" src="the_graph_editor.dart"></script>

Next, generator skipped a `grid` property in `the_grid_editor.dart`, just copy same property from `the_graph.dart`. Check other properties, may be there is something else missed.

Also, if you have previous version of component files, check it and copy additional changes to new files.

#### 4. Move component files

Now, move all files into `lib` folder. We don't need any other folders, let's make paths shorter! After this you should fix all relative paths in html files (just remove `../../` part).

#### 5. Include asserts

Also we need to include all dependencies in this html files. Noflo, klay, hammer, react, font and so on. Let's add follows in each html file:

    <script src="src/react/react-with-addons.js"></script>
    <script src="src/klayjs-noflo/klay-noflo.js"></script>
    <script src="src/hammerjs/hammer.min.js"></script>
    <script src="src/ease-djdeath/index.js"></script>
    <script src="src/react.animate-djdeath/react.animate.js"></script>
    <script src="src/the-graph/build/noflo.js"></script>

#### 6. Check and commit

For checking we should run `example.html` to ensure we haven't any warnings and errors. Commit all changes!

# Possible problems
 
## Polymer doesn't work at all!

Looks like you miss generate 'right' `polymer.html`. Check `lib/src/polyemr/polymer.html` file. It should have follows content:

    <!--
    Copyright (c) 2014 The Polymer Project Authors. All rights reserved.
    This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
    The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
    The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
    Code distributed by Google as part of the polymer project is also
    subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
    -->
    
    <link rel="import" href="../../../../packages/web_components/interop_support.html">
    <!-- Dart note: load polymer for Dart and JS from the same place -->
    <link rel="import" href="../../../../packages/polymer/polymer.html">
    <script>
        // This empty script is here to workaround issue
        // https://github.com/dart-lang/core-elements/issues/11
    </script>

But not like this:

    <!--
    Copyright (c) 2014 The Polymer Project Authors. All rights reserved.
    This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
    The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
    The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
    Code distributed by Google as part of the polymer project is also
    subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
    -->
    
    <link rel="import" href="layout.html">
    
    <script src="polymer.js"></script>

You can just replace text, or take a look at unit 2 in 'Generate dart files' section.

[the-graph-fork]: https://github.com/kelegorm/the-graph