# Changelog

## 0.0.3

- Added pan, offsetX, offsetY, scale properties and focusNode method.

## 0.0.1

- Initial version, created by Stagehand
